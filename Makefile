## SVG2PNG: Each is buggy in their own way
## Usage: $(call SVG2PNG,infile,outfile[,width,height,background-color])
##
## ImageMagick: doesn't support transparency
#SVG2PNG = convert $1 -format png $(if $3,-resize $3x)$(if $4,$4) $(if $5,-background $5) $2
## librsvg: doesn't support <style> element
#SVG2PNG = rsvg-convert $1 -o $2 $(if $3,-w $3) $(if $4,-h $4) $(if $5,--background-color=$5)
## Inkscape: messes up gradients
SVG2PNG = inkscape $1 --export-png $2 $(if $3,-w $3) $(if $4,-h $4) $(if $5,-b '$5')

#PNGCRUSH = cp $1 $2
PNGCRUSH = pngcrush $1 $2

default: PHONY all



link.png    = /usr/share/icons/oxygen/base/16x16/emblems/emblem-symbolic-link.png
dir.png     = /usr/share/icons/oxygen/base/16x16/mimetypes/inode-directory.png
file.png    = /usr/share/icons/oxygen/base/16x16/mimetypes/unknown.png
iso.png     = /usr/share/icons/oxygen/base/16x16/devices/media-optical.png
parent.png  = /usr/share/icons/oxygen/base/16x16/actions/go-up.png
pkg.png     = pkg.svg.png
repo.png    = /usr/share/icons/oxygen/base/16x16/places/folder-tar.png
tar.png     = tar.svg.png
torrent.png = /usr/share/icons/oxygen/base/16x16/actions/download.png
txt.png     = /usr/share/icons/oxygen/base/16x16/mimetypes/text-plain.png

# 'tar.svg' is derived from
#   '/usr/share/icons/oxygen/scalable/places/small/16x16/folder-tar.svgz'

# 'pkg.svg' is derived from 'tar.svg'



pngs := $(shell sed -n 's/\.png\s*=.*/.png/p' $(MAKEFILE_LIST))
$(foreach png,$(pngs),$(eval $(png): $($(png)); $$(call PNGCRUSH,$$<,$$@)))

all: PHONY $(pngs) .gitignore
clean: PHONY
	rm -f -- *.svg.png
distclean: PHONY clean
	rm -f -- $(pngs)

.gitignore: $(MAKEFILE_LIST)
	printf '%s\n' '*~' '*.svg.png' $(pngs) | LC_COLLATE=C sort > .gitignore

%.svg.png: %.svg
	$(call SVG2PNG,$<,$@,16)



.PHONY: PHONY
.DELETE_ON_ERROR:
