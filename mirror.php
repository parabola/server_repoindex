<?php
# Copyright © 2016-2017 Luke Shumaker <lukeshu@sbcglobal.net>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the ./COPYING file for more details.

// XXX: uWSGI doesn't set SCRIPT_NAME if php-app is set.
$_SERVER["SCRIPT_NAME"] = '/.fancyindex/mirror.php';

// Configuration
ini_set('default_socket_timeout', 2);
$tier0_url = 'https://repo.parabola.nu';
$mirrors_urls = [ 'https://www.parabola.nu/mirrors/status/tier/1/json'
                 //, 'https://www.parabola.nu/mirrors/status/tier/2/json'
                 ];
function should_force_tier0($filename) {
	return file_exists('/srv/repo/http/'.$filename) || (preg_match("/\.(db|files)(\.tar(\..z)?)?$/" , $filename) == 1);
}

// Automatic configuration
$assetdir = dirname($_SERVER["SCRIPT_FILENAME"]);
$root = $_SERVER["DOCUMENT_ROOT"];
$filename = explode("?", $_SERVER["REQUEST_URI"], 2)[0];

////////////////////////////////////////////////////////////////////////////////

if (!file_exists($root.'/'.$filename)) {
	require $assetdir.'/404.php';
} elseif (is_dir($root.'/'.$filename)) {
	if (substr("$filename", -1, 1) === "/") {
		// Generate an index page
		require $assetdir.'/index.php';
	} else {
		header('Location: '.$filename.'/');
	}
} else {
	$mirror = $tier0_url;
	if (!should_force_tier0($filename)) {
		$ctime = lstat($root.'/'.$filename)['ctime'];
		header('X-Repomirror-C-Time: '.$ctime);
		// TODO: weight by GeoIP or quality or something?
		$mirrors = array();
		foreach ($mirrors_urls as $mirrors_url) {
			$str = file_get_contents($mirrors_url);
			if ($str === false) {
				error_log("REPOMIRROR Warning: read: ".$mirrors_url);
				continue;
			}
			$json = json_decode($str, true);
			if ($json === NULL) {
				error_log("REPOMIRROR Warning: decode: ".$mirrors_url);
				continue;
			}
			foreach ($json['urls'] as $urldata) {
				if (!in_array($urldata['protocol'], ['http', 'https', 'ftp'])) { continue; }
				if (strtotime($urldata['last_sync']) < $ctime) { continue; }
				header('X-Repomirror-Mirror'.count($mirrors).': '.strtotime($urldata['last_sync']).' '.$urldata['url']);
				$mirrors[] = $urldata['url'];
			}
		}
		if (count($mirrors) > 0) {
			$mirror = $mirrors[array_rand($mirrors)];
		}
	}
	$location = $mirror.$filename;
	if ($mirror === $tier0_url) {
		$location .= "?noredirect";
	}
	header('Location: '.$location);
}
